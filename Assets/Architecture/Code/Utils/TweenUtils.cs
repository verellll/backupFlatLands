﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace FlatLands.Architecture
{
    public static class TweenUtils
    {
        public static void DoValue(float startValue, float endValue, float duration, Action<float> onUpdate = null, Action onCompleted = null)
        {
            DoTweenValue(startValue, endValue, duration, onUpdate, onCompleted);
        }
        
        public static TweenerCore<float, float, FloatOptions> DoTweenValue(float startValue, float endValue, float duration, Action<float> onUpdate = null, Action onCompleted = null)
        {
            var curValue = startValue;
            return DOTween.To(() => curValue, setter => curValue = setter, endValue, duration)
                .OnUpdate(() =>
                {
                    onUpdate?.Invoke(curValue);
                })
                .OnComplete(() =>
                {
                    onCompleted?.Invoke();
                });
        }
    }
}