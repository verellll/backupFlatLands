﻿using Cysharp.Threading.Tasks;

namespace FlatLands.Architecture
{
    public abstract class PrimaryAsyncSharedObject : SharedObject
    {
        public  virtual async UniTask AsyncInit() { }
    }
}