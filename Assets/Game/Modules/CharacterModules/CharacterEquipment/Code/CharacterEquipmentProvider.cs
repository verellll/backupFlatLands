﻿using FlatLands.CharacterLocomotion;
using FlatLands.Characters;
using FlatLands.Equipments;
using UnityEngine;

namespace FlatLands.CharacterEquipment
{
    public sealed class CharacterEquipmentProvider : BaseEquipmentProvider, ICharacterProvider
    {
        private CharacterEquipmentConfig _config;
        private CharacterLocomotionProvider _characterLocomotionProvider;

        public CharacterEquipmentProvider(
            CharacterLocomotionProvider characterLocomotionProvider, 
            BaseEquipmentBehaviour behaviour, 
            Animator animator)
            : base(behaviour, animator)
        {
            _characterLocomotionProvider = characterLocomotionProvider;
        }

        public void Init()
        { 
            _config = CharacterEquipmentConfig.Instance;

            UpdateLinkedObjects();
        }

        public void Dispose() 
        {
         
        }

        public void HandleUpdate()
        {
            UpdateInput();
        }

        public void HandleFixedUpdate()
        {
         
        }

        private void UpdateInput()
        {
            foreach (var key in _config.WeaponInputKeys)
            {
                if (Input.GetKeyDown(key))
                {
                    var slotType = _config.GetWeaponInputKey(key);
                    TakeWeapon(slotType);
                }
            }
        }

        private void TakeWeapon(EquipmentSlotType slotType)
        {
            if(!HasSlot(slotType))
                return;

            if(IsSlotEmpty(slotType))
                return;
            
            var itemConfig = GetItemConfigInSlot(slotType);
            var handPair = GetHandPair(itemConfig.HandType);
            if (handPair.HandType != EquipmentHandType.Hands)
            {
                CheckBothHands();
                return;
            }
            
            if (handPair.IsHandEmpty)
            {
                RemoveItemsFromAllHands(CheckBothHands);
            }
            else
            {
                CheckBothHands();
            }

            void CheckBothHands()
            {
                if (IsHandHoldItem(EquipmentHandType.Hands))
                {
                    var bothHandsPair = GetHandPair(EquipmentHandType.Hands);
                    if (bothHandsPair.Config.SlotType == slotType)
                    {
                        TryRemoveFromHand(EquipmentHandType.Hands);
                        return;
                    }

                    TryRemoveFromHand(EquipmentHandType.Hands, SimpleTake);
                }
                else
                {
                    SimpleTake();
                }
            }
            
            void SimpleTake()
            {
                var handType = handPair.HandType;
                if (IsHandHoldItem(handType))
                {
                    var slotTypeInHand = handPair.Config.SlotType;
                    if (slotTypeInHand == slotType)
                    {
                        TryRemoveFromHand(slotTypeInHand, handType);
                        return;
                    }
                
                    TryRemoveFromHand(slotTypeInHand, handType, () =>
                    {
                        TryTakeToHand(slotType, handType);
                    });
                }
                else
                {
                    TryTakeToHand(slotType, handType);
                }
            }
        }

        private void UpdateLinkedObjects()
        {
            foreach (var linkedObject in Behaviour.LinkedObjects)
            {
                if(linkedObject == null 
                   || linkedObject.LinkingObject == null 
                   || linkedObject.LinkingTarget == null)
                    continue;

                linkedObject.LinkingObject.SetParent(linkedObject.LinkingTarget);
                linkedObject.LinkingObject.localPosition = Vector3.zero;
                linkedObject.LinkingObject.localRotation = Quaternion.identity;
            }
        }
    }
}