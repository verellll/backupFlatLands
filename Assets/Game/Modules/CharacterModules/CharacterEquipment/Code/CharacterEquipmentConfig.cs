﻿using System.Collections.Generic;
using System.Linq;
using FlatLands.Architecture;
using FlatLands.Equipments;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.CharacterEquipment
{
    [CreateAssetMenu(
        menuName = "FlatLands/Characters/" + nameof(CharacterEquipmentConfig), 
        fileName = nameof(CharacterEquipmentConfig))]
    public sealed class CharacterEquipmentConfig : SingletonScriptableObject<CharacterEquipmentConfig>
    {
        [SerializeField, BoxGroup("Main Settings")] 
        private Dictionary<KeyCode, EquipmentSlotType> _weaponInputKeys;

        public IReadOnlyList<KeyCode> WeaponInputKeys => _weaponInputKeys.Keys.ToList();

        public EquipmentSlotType GetWeaponInputKey(KeyCode keyType)
        {
            if (!_weaponInputKeys.TryGetValue(keyType, out var slotType))
                return default;

            return slotType;
        }
    }
}