﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FlatLands.CharacterConstraints
{
    [RequireComponent(typeof(Rig))]
    public class CharacterRigGroup : SerializedMonoBehaviour
    {
        private const string RigGroup = "RigGroup";
        
        [SerializeField] private CharacterConstraintGroupType _groupType;
        [SerializeField, ReadOnly] private Rig _rigConstraint;
        [SerializeField, ReadOnly] private List<ICharacterConstraintPart> _constraintParts;

        public CharacterConstraintGroupType GroupType => _groupType;
        public Rig ConstraintRig => _rigConstraint;
        public List<ICharacterConstraintPart> ConstraintParts => _constraintParts;

        public void Refresh()
        {
            gameObject.name = $"[{_groupType}_{RigGroup}]";
            
            _rigConstraint = GetComponent<Rig>();
            _constraintParts = new List<ICharacterConstraintPart>();
            _constraintParts = GetComponentsInChildren<ICharacterConstraintPart>().ToList();

            foreach (var part in _constraintParts)
                part.Refresh();
        }
    }
}