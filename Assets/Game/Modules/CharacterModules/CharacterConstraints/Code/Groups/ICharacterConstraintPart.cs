﻿using UnityEngine.Animations.Rigging;

namespace FlatLands.CharacterConstraints
{
    public interface ICharacterConstraintPart
    {
        public CharacterConstraintPartType PartType { get; }
        public IRigConstraint Constraint { get; }

        public void Refresh();
    }
}