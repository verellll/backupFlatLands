﻿namespace FlatLands.CharacterConstraints
{
    public enum CharacterConstraintGroupType
    {
        Head = 0,
        Spine = 10,
        Hands = 20
    }
}