using System;
using System.Collections.Generic;
using System.Linq;
using FlatLands.Architecture;
using FlatLands.Characters;

namespace FlatLands.CharacterConstraints
{
	public sealed class CharacterConstraintsProvider : ICharacterProvider
	{
		private readonly CharacterConstraintsBehaviour _behaviour;

		//Дефолтные веса для всех элементов рига, которые выставляем в редакторе
		private readonly Dictionary<CharacterConstraintGroupType, (float, Dictionary<CharacterConstraintPartType, float>)> _defaultGroupsValue = new();

		public CharacterConstraintsProvider(CharacterConstraintsBehaviour behaviour)
		{
			_behaviour = behaviour;
			
			foreach (var (groupType, group) in _behaviour.ConstraintGroups)
			{
				var defaultRigWeight = group.ConstraintRig.weight;
				var defaultPartWeights = new Dictionary<CharacterConstraintPartType, float>();
				group.ConstraintParts.ForEach(p => defaultPartWeights[p.PartType] = p.Constraint.weight);
				_defaultGroupsValue[groupType] = (defaultRigWeight, defaultPartWeights);
			}
		}
		
		public void Init() { }

		public void Dispose() { }

		public void HandleUpdate() { }

		public void HandleFixedUpdate() { }

		
#region Set Params

		public void EnableRigLayer(CharacterConstraintGroupType groupType, float duration, Action callback = null)
		{
			SetRigWeight(groupType, 1, duration, callback: callback);
		}
		
		public void DisableRigLayer(CharacterConstraintGroupType groupType, float duration, Action callback = null)
		{
			SetRigWeight(groupType, 0, duration, callback: callback);
		}

		public void SetGroupWeight(CharacterConstraintGroupType groupType, float newValue, float duration, Action callback = null)
		{
			var group = GetGroup(groupType);
			var completedCount = 0;
			var needOperationsCount = group.ConstraintParts.Count + 1;
			
			SetRigWeight(groupType, newValue, duration, HandleStepCompleted);
			foreach (var part in group.ConstraintParts)
			{
				SetConstraintWeight(groupType, part.PartType, newValue, duration, HandleStepCompleted);
			}

			void HandleStepCompleted()
			{
				completedCount++;
				if(completedCount < needOperationsCount)
					return;
				
				callback?.Invoke();
			}
		}
		
		public void SetDefaultGroupWeight(CharacterConstraintGroupType groupType, float duration, Action callback = null)
		{
			var group = GetGroup(groupType);
			var completedCount = 0;
			var needOperationsCount = group.ConstraintParts.Count + 1;
			
			SetDefaultRigWeight(groupType, duration, callback: HandleStepCompleted);
			foreach (var part in group.ConstraintParts)
			{
				SetDefaultConstraintWeight(groupType, part.PartType, duration, HandleStepCompleted);
			}

			void HandleStepCompleted()
			{
				completedCount++;
				if(completedCount < needOperationsCount)
					return;
				
				callback?.Invoke();
			}
		}

		public void SetRigWeight(CharacterConstraintGroupType groupType, float newValue, float duration, Action callback = null)
		{
			var group = GetGroup(groupType);
			TweenUtils.DoValue(group.ConstraintRig.weight, newValue, duration, onCompleted: callback);
		}
		
		public void SetDefaultRigWeight(CharacterConstraintGroupType groupType, float duration, Action callback = null)
		{
			if (!_defaultGroupsValue.TryGetValue(groupType, out var constraintsPair))
			{
				callback?.Invoke();
				return;
			}
			
			var defaultRigValue = constraintsPair.Item1;
			SetRigWeight(groupType, defaultRigValue, duration, callback:callback);
		}
		
		public void SetConstraintWeight(CharacterConstraintGroupType groupType, CharacterConstraintPartType partType, float newValue, float duration, Action callback = null)
		{
			var group = GetConstraint(groupType, partType);
			TweenUtils.DoValue(group.Constraint.weight, newValue, duration, onCompleted: callback);
		}
		
		public void SetDefaultConstraintWeight(CharacterConstraintGroupType groupType, CharacterConstraintPartType partType, float duration, Action callback = null)
		{
			if (!_defaultGroupsValue.TryGetValue(groupType, out var constraintsPair))
			{
				callback?.Invoke();
				return;
			}
			
			if(!constraintsPair.Item2.TryGetValue(partType, out var defaultWeight))
			{
				callback?.Invoke();
				return;
			}

			SetConstraintWeight(groupType, partType, defaultWeight, duration, callback);
		}
		
#endregion
		

#region Get Params

		public T GetConstraint<T>(CharacterConstraintPartType partType) where T: ICharacterConstraintPart
		{
			foreach (var pair in _behaviour.ConstraintGroups)
			{
				var part = pair.Value.ConstraintParts.FirstOrDefault(p => p.PartType == partType);
				if(part == null)
					continue;

				return (T)part;
			}
			
			return default;
		}
		
		public T GetConstraint<T>(CharacterConstraintGroupType groupType, CharacterConstraintPartType partType) where T: ICharacterConstraintPart
		{
			return (T) GetConstraint(groupType, partType);
		}
		
		public ICharacterConstraintPart GetConstraint(CharacterConstraintGroupType groupType, CharacterConstraintPartType partType)
		{
			if (!_behaviour.ConstraintGroups.TryGetValue(groupType, out var constraintGroup))
				return default;

			return constraintGroup.ConstraintParts.FirstOrDefault(p => p.PartType == partType);
		}
		
		public CharacterRigGroup GetGroup(CharacterConstraintGroupType groupType)
		{
			if (!_behaviour.ConstraintGroups.TryGetValue(groupType, out var constraintGroup))
				return default;

			return constraintGroup;
		}
		
#endregion

	}
}
