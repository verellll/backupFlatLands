﻿using System.Collections.Generic;
using FlatLands.Characters;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.CharacterConstraints
{
    public sealed class CharacterConstraintsBehaviour : SerializedMonoBehaviour, ICharacterBehaviour
    {
        [SerializeField] 
        private Dictionary<CharacterConstraintGroupType, CharacterRigGroup> _constraintGroups;
        
        public Transform EntityTransform => transform;

        public IReadOnlyDictionary<CharacterConstraintGroupType, CharacterRigGroup> ConstraintGroups => _constraintGroups;

        [Button]
        private void Refresh()
        {
            _constraintGroups = new Dictionary<CharacterConstraintGroupType, CharacterRigGroup>();
            var groups = transform.GetComponentsInChildren<CharacterRigGroup>();
            foreach (var group in groups)
            {
                group.Refresh();
                _constraintGroups[group.GroupType] = group;
            }
        }
    }
}