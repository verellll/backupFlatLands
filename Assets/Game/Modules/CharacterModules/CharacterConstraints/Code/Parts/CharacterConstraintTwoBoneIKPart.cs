﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FlatLands.CharacterConstraints
{
    [RequireComponent(typeof(TwoBoneIKConstraint))]
    public sealed class CharacterConstraintTwoBoneIKPart : SerializedMonoBehaviour, ICharacterConstraintPart
    {
        private const string RigPart = "RigPart";
        private const string RigTarget = "RigTarget";
        private const string RigHint = "RigHint";

        [SerializeField] private CharacterConstraintPartType _partType;
        [SerializeField, ReadOnly] private TwoBoneIKConstraint _twoBoneIKConstraint;

        public Transform Target => _twoBoneIKConstraint.data.target;
        public Transform Hint => _twoBoneIKConstraint.data.hint;
        
        public CharacterConstraintPartType PartType => _partType;
        public IRigConstraint Constraint => _twoBoneIKConstraint;

        
        public void Refresh()
        {
            gameObject.name = $"[{_partType}_{RigPart}]";
            _twoBoneIKConstraint = GetComponent<TwoBoneIKConstraint>();

            if(_twoBoneIKConstraint != null || Target != null)
                Target.name = $"{_partType}_{RigTarget}";
            
            if(_twoBoneIKConstraint != null || Hint != null)
                Hint.name = $"{_partType}_{RigHint}";
        }
    }
}