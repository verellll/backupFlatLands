﻿namespace FlatLands.CharacterConstraints
{
    public enum CharacterConstraintPartType
    {
        Head = 0,
        
        SpineLower = 10,
        SpineMiddle = 11,
        SpineUpper = 12,
        
        HandLeft = 20,
        HandRight = 21,
    }
}