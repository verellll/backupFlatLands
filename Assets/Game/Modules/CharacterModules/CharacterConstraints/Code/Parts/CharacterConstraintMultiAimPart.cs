﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FlatLands.CharacterConstraints
{
    [RequireComponent(typeof(MultiAimConstraint))]
    public sealed class CharacterConstraintMultiAimPart : SerializedMonoBehaviour, ICharacterConstraintPart
    {
        private const string RigPart = "RigPart";
        
        [SerializeField] private CharacterConstraintPartType _partType;
        [SerializeField, ReadOnly] private MultiAimConstraint _multiAimConstraint;
        
        public CharacterConstraintPartType PartType => _partType;
        public IRigConstraint Constraint => _multiAimConstraint;

        public void Refresh()
        {
            gameObject.name = $"[{_partType}_{RigPart}]";
            _multiAimConstraint = GetComponent<MultiAimConstraint>();
        }
    }
}