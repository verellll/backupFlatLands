﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FlatLands.CharacterConstraints
{
    [RequireComponent(typeof(MultiRotationConstraint))]
    public sealed class CharacterConstraintMultiRotationPart : SerializedMonoBehaviour, ICharacterConstraintPart
    {
        private const string RigPart = "RigPart";
        
        [SerializeField] private CharacterConstraintPartType _partType;
        [SerializeField, ReadOnly] private MultiRotationConstraint _multiRotationConstraint;
        
        public CharacterConstraintPartType PartType => _partType;
        public IRigConstraint Constraint => _multiRotationConstraint;

        public void Refresh()
        {
            gameObject.name = $"[{_partType}_{RigPart}]";
            _multiRotationConstraint = GetComponent<MultiRotationConstraint>();
        }
    }
}