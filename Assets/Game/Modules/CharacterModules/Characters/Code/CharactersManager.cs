﻿using System;
using FlatLands.Architecture;
using FlatLands.EntityControllable;
using FlatLands.Locations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace FlatLands.Characters
{
    public sealed class CharactersManager : SharedObject
    {
        [Inject] private EntityControllableManager _controllableManager;
        [Inject] private LocationsManager _locationsManager;
        
        public CharacterGroup CurrentCharacter { get; private set; }
        public event Action OnCharacterCreated;

        private GeneralCharacterConfig _config;
        
        public override void Init()
        {
            _config = GeneralCharacterConfig.Instance;
        }

        public override void Dispose()
        {
            
        }

        public void CreateDefaultCharacter()
        {
            var prefab = _config.DefaultCharacterPrefab;
            var characterLocationData = _locationsManager.CurLocation.Item2.GetData<CharactersLocationData>();
            var behaviour = Object.Instantiate(prefab, characterLocationData.SpawnPoint.position, Quaternion.identity);
            CurrentCharacter = new CharacterGroup(behaviour);
            _container.InjectAt(CurrentCharacter);
            OnCharacterCreated?.Invoke();
        }
    }
}