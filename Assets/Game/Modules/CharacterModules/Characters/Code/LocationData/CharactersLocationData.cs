﻿using FlatLands.Locations;
using UnityEngine;

namespace FlatLands.Characters
{
    public sealed class CharactersLocationData : ILocationData
    {
        [SerializeField] private Transform _spawnPoint;

        public Transform SpawnPoint => _spawnPoint;
        
        public void Refresh(GameObject hierarchyObject) { }
    }
}