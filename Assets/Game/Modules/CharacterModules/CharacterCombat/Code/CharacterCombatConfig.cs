﻿using FlatLands.CombatSystem;
using UnityEngine;

namespace FlatLands.CharacterCombat
{
    [CreateAssetMenu(
        menuName = "FlatLands/Characters/" + nameof(CharacterCombatConfig), 
        fileName = nameof(CharacterCombatConfig))]
    public sealed class CharacterCombatConfig : BaseCombatConfig
    {

    }
}