﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FlatLands.Architecture;
using UnityEngine;

namespace FlatLands.Equipments
{
    public abstract class BaseEquipmentProvider
    {
        public IReadOnlyDictionary<EquipmentSlotType, (BaseEquipmentItemConfig, BaseEquipmentItemBehaviour)> EquipmentSlots => _equipmentSlots;
        
        private readonly Dictionary<EquipmentSlotType, (BaseEquipmentItemConfig, BaseEquipmentItemBehaviour)> _equipmentSlots;
        private readonly Dictionary<EquipmentHandType, EquipmentHandPair> _handPairs;

        protected readonly BaseEquipmentBehaviour Behaviour;
        protected readonly Animator _animator;

        public event Action<EquipmentHandType> OnItemAddToHand;
        public event Action<BaseEquipmentItemConfig, BaseEquipmentItemBehaviour> OnItemPairAddToHand;
        public event Action<EquipmentHandType> OnItemRemoveFromHand;
        
        protected BaseEquipmentProvider(BaseEquipmentBehaviour behaviour, Animator animator)
        {
            _equipmentSlots = new Dictionary<EquipmentSlotType, (BaseEquipmentItemConfig, BaseEquipmentItemBehaviour)>();
            
            Behaviour = behaviour;
            _animator = animator;
            
            var slots = Behaviour.GetAllSlots();
            foreach (var slot in slots)
            {
                _equipmentSlots[slot] = (null, null);
            }

            _handPairs = new Dictionary<EquipmentHandType, EquipmentHandPair>()
            {
                {EquipmentHandType.LeftHand, new EquipmentHandPair(EquipmentHandType.LeftHand)},
                {EquipmentHandType.RightHand, new EquipmentHandPair(EquipmentHandType.RightHand)},
                {EquipmentHandType.Hands, new EquipmentHandPair(EquipmentHandType.Hands)},
            };
        }
        
        
#region Slot Items

        public bool HasSlot(EquipmentSlotType slotType)
        {
            return _equipmentSlots.ContainsKey(slotType);
        }

        public bool IsSlotEmpty(EquipmentSlotType slotType)
        {
            var config = GetItemConfigInSlot(slotType);
            var behaviour = GetItemBehaviourInSlot(slotType);
            return config == null || behaviour == null;
        }

        public BaseEquipmentItemConfig GetItemConfigInSlot(EquipmentSlotType slotType)
        {
            if (!_equipmentSlots.TryGetValue(slotType, out var item))
                return default;

            return item.Item1;
        }

        public BaseEquipmentItemBehaviour GetItemBehaviourInSlot(EquipmentSlotType slotType)
        {
            if (!_equipmentSlots.TryGetValue(slotType, out var item))
                return default;

            return item.Item2;
        }

        public EquipmentSlotBehaviour GetSlotBehaviour(EquipmentSlotType slotType)
        {
            return Behaviour.GetBehaviour(slotType);
        }

        public void SetItemToSlot(EquipmentSlotType slotType, BaseEquipmentItemConfig newItemConfig, BaseEquipmentItemBehaviour newItemBehaviour)
        {
            _equipmentSlots[slotType] = (newItemConfig, newItemBehaviour);
        }
        
        protected bool IsHandHoldItem(EquipmentHandType handType)
        {
            var handPair = GetHandPair(handType);
            return !handPair.IsHandEmpty;
        }

        protected List<EquipmentHandPair> GetHandsWithItems()
        {
            var hands = new List<EquipmentHandPair>();
            foreach (var handPair in _handPairs)
                if(!handPair.Value.IsHandEmpty)
                    hands.Add(handPair.Value);
            
            return hands;
        }
        
        protected EquipmentHandPair GetHoldingItemHand(EquipmentSlotType slotType)
        {
            foreach (var handPair in _handPairs)
            {
                if(handPair.Value.IsHandEmpty)
                    continue;

                if (handPair.Value.Config.SlotType == slotType)
                    return handPair.Value;
            }

            return default;
        }
        
        protected EquipmentHandPair GetHandPair(EquipmentHandType handType)
        {
            if (!_handPairs.TryGetValue(handType, out var handPair))
                return default;

            return handPair;
        }
        
        protected EquipmentHandPair GetHandPair(EquipmentSlotType slotType)
        {
            foreach (var pair in _handPairs)
                if (pair.Value.Config.SlotType == slotType)
                    return pair.Value;

            return default;
        }
        
        protected EquipmentHandPair GetActivePair(EquipmentSlotType slotType)
        {
            foreach (var pair in _handPairs)
            {
                if(pair.Value.IsHandEmpty)
                    continue;

                if (pair.Value.Config.SlotType == slotType)
                    return pair.Value;
            }

            return default;
        }

#endregion


#region Take

        protected bool TryTakeToHand(EquipmentSlotType slotType, EquipmentHandType handType, Action callback = null)
        {
            var slotEmpty = IsSlotEmpty(slotType);
            var handPair = GetHandPair(handType);
            var hasEquipment = _equipmentSlots.TryGetValue(slotType, out var equipmentPair);
            
            if(slotEmpty || !handPair.IsHandEmpty || handPair.IsRoutineActive || !hasEquipment)
            {
                callback?.Invoke();
                return false;
            }

            handPair.SetItem(equipmentPair.Item1, equipmentPair.Item2);
            
            var animLayer = handPair.Config.AnimatorLayer;
            var routine = _animator.PlayAnimationWithAction(
                animLayer,
                handPair.Config.TakeToHandName, 
                0.5f,
                HandleAnimAction,
                HandAnimEnd);
            
            handPair.SetRoutine(routine);
            UnityEventsProvider.CoroutineStart(routine);
            return true;
            
            void HandleAnimAction()
            {
                var mainHandTransform = handType == EquipmentHandType.RightHand
                    ? Behaviour.RightHandHolder
                    : Behaviour.LeftHandHolder;

                var posInHand = handPair.Config.PosInHand;
                var rotInHand = handPair.Config.RotInHand;
                
                handPair.Behaviour.transform.SetParent(mainHandTransform, false);
                handPair.Behaviour.transform.DOLocalMove(posInHand, 0.2f);
                handPair.Behaviour.transform.DOLocalRotate(rotInHand, 0.2f);
            }

            void HandAnimEnd()
            {
                handPair.SetRoutine(null);
                callback?.Invoke();
                OnItemAddToHand?.Invoke(handType);
                OnItemPairAddToHand?.Invoke(handPair.Config, handPair.Behaviour);
            }
        }

#endregion


#region Remove

        protected void RemoveItemsFromAllHands(Action callback = null)
        {
            var hands = new List<EquipmentHandType>()
            {
                EquipmentHandType.LeftHand,
                EquipmentHandType.RightHand,
                EquipmentHandType.Hands
            };
            
            RemoveItemsFromHands(hands, callback);
        }

        protected void RemoveItemsFromHands(List<EquipmentHandType> handTypes, Action callback = null)
        {
            var needToRemoveCount = handTypes.Count;
            foreach (var handType in handTypes)
            {
                TryRemoveFromHand(handType, HandleUpdateRemoved);
            }

            void HandleUpdateRemoved()
            {
                needToRemoveCount--;
                if (needToRemoveCount <= 0)
                    callback?.Invoke();
            }
        }
        
        protected bool TryRemoveFromHand(EquipmentSlotType slotType, Action callback = null)
        {
            var handWithTargetSlotType = GetHoldingItemHand(slotType);
            if (IsSlotEmpty(slotType) || handWithTargetSlotType == null)
            {
                callback?.Invoke();
                return false;
            }

            return TryRemoveFromHand(slotType, handWithTargetSlotType.HandType, callback);
        }
        
        protected bool TryRemoveFromHand(EquipmentHandType handType, Action callback = null)
        {
            var handPair = GetHandPair(handType);
            if(handPair.IsHandEmpty || handPair.IsRoutineActive)
            {
                callback?.Invoke();
                return false;
            }

            return TryRemoveFromHand(handPair.Config.SlotType, handType, callback);
        }
        
        protected bool TryRemoveFromHand(EquipmentSlotType slotType, EquipmentHandType handType, Action callback = null)
        {
            var handPair = GetHandPair(handType);
            if(handPair.IsHandEmpty || handPair.IsRoutineActive)
            {
                callback?.Invoke();
                return false;
            }

            var animLayer = handPair.Config.AnimatorLayer;
            var routine = _animator.PlayAnimationWithAction(
                animLayer,
                handPair.Config.PutFromHandName, 
                0.5f,
                HandleAnimAction, 
                HandAnimEnd);
            
            handPair.SetRoutine(routine);
            UnityEventsProvider.CoroutineStart(routine);
            return true;
            
            void HandleAnimAction()
            {
                var pivotParent = GetSlotBehaviour(slotType).PivotTrans;
                var posOnBody = handPair.Config.PosOnBody;
                var rotOnBody = handPair.Config.RotOnBody;
                    
                handPair.Behaviour.transform.SetParent(pivotParent.transform);
                handPair.Behaviour.transform.DOLocalMove(posOnBody, 0.2f);
                handPair.Behaviour.transform.DOLocalRotate(rotOnBody, 0.2f);
            }

            void HandAnimEnd()
            {
                handPair.SetRoutine(null);
                handPair.SetItem(null, null);
                callback?.Invoke();
                OnItemRemoveFromHand?.Invoke(handType);
            }
        }

#endregion

    }
    
    public sealed class EquipmentHandPair
    {
        public EquipmentHandType HandType { get; }
        public bool IsHandEmpty => _itemConfig == null || _itemBehaviour == null;
        public bool IsRoutineActive => _handRoutine != null;
        
        public BaseEquipmentItemConfig Config => _itemConfig;
        public BaseEquipmentItemBehaviour Behaviour => _itemBehaviour;
        
        private BaseEquipmentItemConfig _itemConfig;
        private BaseEquipmentItemBehaviour _itemBehaviour;

        private IEnumerator _handRoutine;

        public EquipmentHandPair(EquipmentHandType handType)
        {
            HandType = handType;
        }
        
        public void SetItem(BaseEquipmentItemConfig config, BaseEquipmentItemBehaviour behaviour)
        {
            _itemConfig = config;
            _itemBehaviour = behaviour;
        }

        public void SetRoutine(IEnumerator routine)
        {
            _handRoutine = routine;
        }
    }
}