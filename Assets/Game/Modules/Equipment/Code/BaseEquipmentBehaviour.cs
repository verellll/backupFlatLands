using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FlatLands.Equipments
{
	public abstract class BaseEquipmentBehaviour : SerializedMonoBehaviour
	{
		[SerializeField] private Transform _leftHandHolder;
		[SerializeField] private Transform _rightHandHolder;
		
		[SerializeField, ReadOnly] 
		private Dictionary<EquipmentSlotType, EquipmentSlotBehaviour> _equipmentSlotsBehaviourPairs;

		[SerializeField, ReadOnly] 
		private List<ILinkedObject> _linkedObjects;

		public Transform LeftHandHolder => _leftHandHolder;
		public Transform RightHandHolder => _rightHandHolder;

		public IReadOnlyList<ILinkedObject> LinkedObjects => _linkedObjects;

		public EquipmentSlotBehaviour GetBehaviour(EquipmentSlotType type)
		{
			if (!_equipmentSlotsBehaviourPairs.TryGetValue(type, out var behaviour))
				return default;

			return behaviour;
		}

		public List<EquipmentSlotType> GetAllSlots()
		{
			return _equipmentSlotsBehaviourPairs.Keys.ToList();
		}
		

#if UNITY_EDITOR
		
		[Button]
		private void RefreshSlots()
		{
			var behaviours = gameObject.GetComponentsInChildren<EquipmentSlotBehaviour>();
			_equipmentSlotsBehaviourPairs = new Dictionary<EquipmentSlotType, EquipmentSlotBehaviour>();
			foreach (var slotBehaviour in behaviours)
			{
				_equipmentSlotsBehaviourPairs[slotBehaviour.SlotType] = slotBehaviour;
			}

			var linkedObjects = gameObject.GetComponentsInChildren<ILinkedObject>();
			_linkedObjects = new List<ILinkedObject>();
			_linkedObjects.AddRange(linkedObjects);
		}
#endif
		
	}
}
