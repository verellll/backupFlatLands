﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.Equipments
{
    public sealed class EquipmentSlotBehaviour : SerializedMonoBehaviour, ILinkedObject
    {
        [SerializeField] private Transform _linkedTarget;
        
        
        [SerializeField] private EquipmentSlotType _slotType;
        [SerializeField] private Transform _pivotTransform;

        public EquipmentSlotType SlotType => _slotType;
        public Transform PivotTrans => _pivotTransform;

#region ILinkedObject

        public Transform LinkingObject => transform;
        public Transform LinkingTarget => _linkedTarget;

#endregion

    }
}