﻿using UnityEngine;

namespace FlatLands.Equipments
{
    public abstract class BaseEquipmentItemController
    {
        public BaseEquipmentItemConfig Config { get; }
        public BaseEquipmentItemBehaviour Behaviour { get; }
        public Animator MainAnimator { get; }

        public BaseEquipmentItemController(BaseEquipmentItemConfig config, BaseEquipmentItemBehaviour behaviour, Animator animator)
        {
            Config = config;
            Behaviour = behaviour;
            MainAnimator = animator;
        }
        
        public virtual void Init(){ }
        public virtual void Dispose(){ }
        
        public virtual void MouseButtonHold(MouseButtonType buttonType){ }

        public virtual void MouseButtonDown(MouseButtonType buttonType){ }

        public virtual void MouseButtonUp(MouseButtonType buttonType){ }

        public virtual void HandleUpdate() { }
    }
}