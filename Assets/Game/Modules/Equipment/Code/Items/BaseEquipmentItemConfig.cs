﻿using System;
using FlatLands.Architecture;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.Equipments
{
    public abstract class BaseEquipmentItemConfig : MultitonScriptableObjectsByName<BaseEquipmentItemConfig>
    {
        [SerializeField, BoxGroup("Main Settings")] 
        private BaseEquipmentItemBehaviour baseEquipmentItemPrefab;
        
        [SerializeField, BoxGroup("Main Settings")] 
        private EquipmentSlotType _equipmentSlotType;
        
        [SerializeField, BoxGroup("Main Settings")]
        private EquipmentHandType _handType;

        [SerializeField, FoldoutGroup("Placement Settings")]
        private Vector3 _posOnBody;
        
        [SerializeField, FoldoutGroup("Placement Settings")]
        private Vector3 _rotOnBody;
        
        [SerializeField, FoldoutGroup("Placement Settings"), Space]
        private Vector3 _posInHand;
        
        [SerializeField, FoldoutGroup("Placement Settings")]
        private Vector3 _rotInHand;
        
        [SerializeField, FoldoutGroup("Animator Settings")] 
        private string _animatorLayer;
        
        [SerializeField, FoldoutGroup("Animator Settings"), Space] 
        private string _takeToHandName;
        
        [SerializeField, FoldoutGroup("Animator Settings")] 
        private string _putFromHandName;

        public BaseEquipmentItemBehaviour BaseEquipmentItemPrefab => baseEquipmentItemPrefab;
        public EquipmentSlotType SlotType => _equipmentSlotType;

        public Vector3 PosOnBody => _posOnBody;
        public Vector3 RotOnBody => _rotOnBody;

        public EquipmentHandType HandType => _handType;
        public Vector3 PosInHand => _posInHand;
        public Vector3 RotInHand => _rotInHand;
        
        public string AnimatorLayer => _animatorLayer;
        public string TakeToHandName => _takeToHandName;
        public string PutFromHandName => _putFromHandName;

        public abstract BaseEquipmentItemController CreateInstanceController(BaseEquipmentItemBehaviour behaviour, Animator animator);
    }

    [Flags]
    public enum EquipmentHandType
    {
        RightHand = 0b00000001,
        LeftHand = 0b00000010,
        Hands = 0b00000100,
    }
}