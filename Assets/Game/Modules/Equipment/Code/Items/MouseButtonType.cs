﻿namespace FlatLands.Equipments
{
    public enum MouseButtonType
    {
        LeftButton = 0,
        RightButton = 1,
        MiddleButton = 2,
    }
}