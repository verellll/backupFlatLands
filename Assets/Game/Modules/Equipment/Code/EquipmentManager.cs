﻿using System;
using FlatLands.Architecture;
using UnityEngine;

namespace FlatLands.Equipments
{
    public sealed class EquipmentManager : SharedObject
    {
        public event Action<EquipmentSlotType> OnEquipmentItemAdded;
        public event Action<EquipmentSlotType> OnEquipmentItemRemoved;
        
        public void AddEquipmentItemToProvider(BaseEquipmentItemConfig equipmentItemConfig, BaseEquipmentProvider targetProvider)
        {
            if(equipmentItemConfig == null 
               || targetProvider == null)
                return;

            var slotType = equipmentItemConfig.SlotType;
            if(!targetProvider.HasSlot(slotType))
                return;
            
            var isSlotEmpty = targetProvider.IsSlotEmpty(slotType);
            if (!isSlotEmpty)
                RemoveEquipmentItemFromProvider(slotType, targetProvider);

            var pivotParent = targetProvider.GetSlotBehaviour(slotType).PivotTrans;
            var createdItem = GameObject.Instantiate(equipmentItemConfig.BaseEquipmentItemPrefab, pivotParent, false);
            createdItem.transform.localPosition = equipmentItemConfig.PosOnBody;
            createdItem.transform.localRotation = Quaternion.Euler(equipmentItemConfig.RotOnBody);
            
            targetProvider.SetItemToSlot(slotType, equipmentItemConfig, createdItem);
            OnEquipmentItemAdded?.Invoke(slotType);
        }
        
        public void RemoveEquipmentItemFromProvider(EquipmentSlotType slotType, BaseEquipmentProvider targetProvider)
        {
            if(targetProvider == null)
                return;
            
            if(!targetProvider.HasSlot(slotType))
                return;
            
            var isSlotEmpty = targetProvider.IsSlotEmpty(slotType);
            if (isSlotEmpty)
                return;

            var itemInSlot = targetProvider.GetItemBehaviourInSlot(slotType);
            GameObject.Destroy(itemInSlot.gameObject);
            targetProvider.SetItemToSlot(slotType, null, null);

            OnEquipmentItemRemoved?.Invoke(slotType);
        }
    }
}