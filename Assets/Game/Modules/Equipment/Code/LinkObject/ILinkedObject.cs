﻿using UnityEngine;

namespace FlatLands.Equipments
{
    //Нужен для привязки пустышек и слотов к разным частям тела персонажа
    //Путем смены родителя
    public interface ILinkedObject
    {
        //Объект который привязываем
        public Transform LinkingObject { get; }
        
        //Цель к которой привязываем
        public Transform LinkingTarget { get; }
    }
}