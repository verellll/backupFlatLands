﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.Equipments
{
    public class GenericLinkedObject : SerializedMonoBehaviour, ILinkedObject
    {
        [SerializeField, FoldoutGroup("Linked Settings")] 
        private Transform _linkingTarget;
        
        public Transform LinkingObject => transform;
        public Transform LinkingTarget => _linkingTarget;
    }
}