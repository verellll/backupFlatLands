﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using FlatLands.Architecture;
using FlatLands.Cursors;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace FlatLands.GeneralCamera
{
    public sealed class GeneralCameraManager : SharedObject
    {
        [Inject] private CursorManager _cursorManager;

        private Dictionary<string, Camera> _overlayCameras;

        public CameraHierarchy Hierarchy { get; private set; }
        public bool IsActive { get; private set; }

        public float NormalizedXAngle { get; private set; }
        
        public event Action<RaycastHit> OnHit;

        private Transform _cameraTarget;
        private GeneralCameraConfig _config;
        
        private Vector3 _cameraFollowVelocity;
        private float _pivotFollowVelocity;

        private float _mouseX;
        private float _mouseY;
        
        private float _smoothX;
        private float _smoothY;

        private float _smoothXVelocity;
        private float _smoothYVelocity;

        private float _lookAngle;
        private float _titleAngle;

        private float _currentFollowSpeed;

        private Sequence _pivotSequence;
        private Sequence _pivotXOffsetSequence;
        private Sequence _zoomSequence;

        internal void InvokeCameraCreated(CameraHierarchy hierarchy)
        {
            Hierarchy = hierarchy;
        }
        
        public override void Init()
        {
            _overlayCameras = new Dictionary<string, Camera>();
            _config = GeneralCameraConfig.Instance;
            
            RegisterOverlayCameras();
            ApplyOverlayCamerasToGeneral();

            _cursorManager.OnCursorStateChanged += HandleCursorStateChanged;
            UnityEventsProvider.OnFixedUpdate += OnFixedUpdate;
            SetDefaultXOffset();
            SetDefaultZoom();
            SetDefaultFollowSpeed();
            SetDefaultPivotPos();
            IsActive = true;
        }

        public override void Dispose()
        {
            _cursorManager.OnCursorStateChanged -= HandleCursorStateChanged;
            UnityEventsProvider.OnFixedUpdate -= OnFixedUpdate;
        }

        private void OnFixedUpdate()
        {
            UpdateCameraMovement();
            UpdateHits();
        }

        private void HandleCursorStateChanged()
        {
            IsActive = !_cursorManager.CursorActive;
        }

#region Main

        private void RegisterOverlayCameras()
        {
            var cameras = _container.GetAll<IOverlayCameraHolder>();
            foreach (var holders in cameras)
            {
                if(holders.GetOverlayCamera == null)
                    continue;
                
                _overlayCameras[holders.Id] = holders.GetOverlayCamera;
            }
        }

        private void ApplyOverlayCamerasToGeneral()
        {
            var cameraData = Hierarchy.CameraComponent.GetUniversalAdditionalCameraData();
            foreach (var pair in _overlayCameras)
            {
                cameraData.cameraStack.Add(pair.Value);
            }
        }
        
        public void SetCameraTarget(Transform target)
        {
            _cameraTarget = target;
        }

        public void SetCameraActive(bool active)
        {
            IsActive = active;
        }

        //Нормализованное значние наклона камеры по вертикали от -1 до 1 
        public float GetNormalizedTitleAngle()
        {
            var minValue = _config.AngleLimit.y;
            var maxValue = _config.AngleLimit.x;

            return (_titleAngle - minValue) * 2 / (maxValue - minValue) - 1;
        }

#endregion


#region Movements

        private void UpdateCameraMovement()
        {
            if(_cameraTarget == null)
                return;

            UpdateInput();
            UpdateCameraPosition();
            UpdateCameraRotation();
            NormalizeXLookAngle();
        }

        private void UpdateInput()
        {
            if(!IsActive)
            {
                _mouseX = 0;
                _mouseY = 0;
                return;
            }

            _mouseX = Input.GetAxis("Mouse X");
            _mouseY = Input.GetAxis("Mouse Y");
        }
        
        private void UpdateCameraPosition()
        {
            var newCameraPos = Vector3.SmoothDamp(Hierarchy.transform.position, _cameraTarget.transform.position,
                ref _cameraFollowVelocity, _currentFollowSpeed);
            
            Hierarchy.transform.position = newCameraPos;
        }

        private void UpdateCameraRotation()
        {
            var turnSmooth = _config.TurnSmooth;
           
           if (turnSmooth > 0)
           {
               _smoothX = Mathf.SmoothDamp(_smoothX, _mouseX, ref _smoothXVelocity, turnSmooth);
               _smoothY = Mathf.SmoothDamp(_smoothY, _mouseY, ref _smoothYVelocity, turnSmooth);
           }
           else
           {
               _smoothX = _mouseX;
               _smoothY = _mouseY;
           }

           _lookAngle += _smoothX * _config.HorizontalRotationSpeed;
           Hierarchy.transform.rotation = Quaternion.Euler(0, _lookAngle, 0);

           _titleAngle -= _smoothY *  _config.VerticalRotationSpeed;
           _titleAngle = Mathf.Clamp(_titleAngle,  _config.AngleLimit.x,  _config.AngleLimit.y);
           
           Hierarchy.Pivot.localRotation = Quaternion.Euler(_titleAngle, 0, 0);
        }

        //Нормализованый угол обзор по оси X
        //1 - смотрит вверх
        //-1 смотрит вниз
        private void NormalizeXLookAngle()
        {
            var min = _config.AngleLimit.x;
            var max = _config.AngleLimit.y;
            var normalizedX = 2 * (_titleAngle - min) / (max - min) - 1;
            NormalizedXAngle = -normalizedX;
        }
        
#endregion


#region Hits

        private void UpdateHits()
        {
            var cameraTransform = Hierarchy.CameraComponent.transform;
            var startPos = cameraTransform.position;
            var startDirection = cameraTransform.forward;

            var ray = new Ray(startPos, startDirection);
            RaycastHit hit;
            Physics.SphereCast(ray, 0.2f, out hit, 10, ~_config.IgnoreHitLayers);

            OnHit?.Invoke(hit);

#if UNITY_EDITOR
            Hierarchy.SetDebugHit(10);
#endif
            
        }

#endregion


#region Changes by Conditions

        public void SetNewPivotPos(Vector3 newPivotPos, float duration, Action completed = null)
        {
            SetPivotPos(newPivotPos, duration, completed);
        }
        
        public void SetDefaultPivotPos()
        {
            SetPivotPos(_config.DefaultPivotPos, 0.1f);
        }

        public void SetCustomFollowSpeed(float newSpeed)
        {
            _currentFollowSpeed = newSpeed;
        }
                
        public void SetDefaultFollowSpeed()
        {
            _currentFollowSpeed = _config.FollowSpeed;
        }

        public void SetCustomXOffset(float xOffsetPos, float duration, Action completed = null)
        {
            SetXOffset(xOffsetPos, duration, completed);
        }
                
        public void SetDefaultXOffset()
        {
            SetXOffset(_config.PivotXOffset, _config.PivotXOffsetDuration);
        }

        private void SetXOffset(float xOffsetPos, float duration, Action completed = null)
        {
            _pivotXOffsetSequence?.Kill();
            _pivotXOffsetSequence = DOTween.Sequence();
            _pivotXOffsetSequence.Append(Hierarchy.XOffset.DOLocalMove(new Vector3(xOffsetPos, 0, 0), duration));
            _pivotXOffsetSequence.OnComplete(() =>
            {
                _pivotXOffsetSequence?.Kill();
                completed?.Invoke();
            });
        }

        public void SetCustomZoom(float newZoom, float duration, Action completed = null)
        {
            SetZoom(newZoom, duration, completed);
        }
                        
        public void SetDefaultZoom()
        {
            SetZoom(_config.ZoomDefault, _config.ZoomDuration);
        }

        private void SetZoom(float newZoom, float duration, Action completed = null)
        {
            _zoomSequence?.Kill();
            _zoomSequence = DOTween.Sequence();
            _zoomSequence.Append(Hierarchy.CameraComponent.transform.DOLocalMove(new Vector3(0, 0, newZoom), duration));
            _zoomSequence.OnComplete(() =>
            {
                _zoomSequence?.Kill();
                completed?.Invoke();
            });
        }

        private void SetPivotPos(Vector3 newPos, float duration, Action completed =  null)
        {
            _pivotSequence?.Kill();
            _pivotSequence = DOTween.Sequence();
            _pivotSequence.Append(Hierarchy.Pivot.transform.DOLocalMove(newPos, duration));
            _pivotSequence.OnComplete(() =>
            {
                _pivotSequence?.Kill();
                completed?.Invoke();
            });
        }

#endregion

    }
}