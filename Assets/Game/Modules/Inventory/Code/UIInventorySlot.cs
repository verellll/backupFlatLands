﻿using FlatLands.Items;

namespace FlatLands.Inventory
{
    public class UIInventorySlot : UIItemSlot
    {
        public override bool CanDrag => true;
    }
}