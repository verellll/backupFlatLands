using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FlatLands.Architecture;
using FlatLands.Items;
using FlatLands.UI;
using UnityEngine;

namespace FlatLands.Inventory
{
    public class UIInventoryWindow : UIWindow
    {
        [Inject] private InventoryModel _inventoryModel;
        
        [SerializeField]
        private UIInventorySlot _prefab;
        
        [SerializeField]
        private Transform _content;

        private List<UIInventorySlot> _inventorySlots;

        public override void Init()
        {
            _inventorySlots = new List<UIInventorySlot>();
            _inventoryModel.OnSlotsChanged += HandleSlotsChanged;
            _inventoryModel.OnItemsChanged += HandleItemsChanged;
        }

        public override void Dispose()
        {
            _inventoryModel.OnSlotsChanged -= HandleSlotsChanged;
            _inventoryModel.OnItemsChanged -= HandleItemsChanged;
        }

        private void HandleSlotsChanged()
        {
            var slotsCount = _inventoryModel.SlotsCount;
            var viewsCount = _inventorySlots.Count;
            var needCount = Mathf.Abs(slotsCount - viewsCount);//число по модулю

            if (slotsCount == viewsCount)
            {
                UpdateSlots();
                return;
            }

            if (slotsCount > viewsCount)
            {
                for (int i = 0; i < needCount; i++)
                {
                    CreateSlot();
                }
            }
            else
            {
                for (int i = 0; i < needCount; i++)
                {
                    var lastView = _inventorySlots.Last();
                    DestroySlot(lastView);
                }
            }

            UpdateSlots();
        }

        private void HandleItemsChanged()
        {
            UpdateSlots();
        }
        
        private UIInventorySlot CreateSlot()
        {
            var createdSlot = Instantiate(_prefab, _content);
            _inventorySlots.Add(createdSlot);
            GlobalContainer.InjectAt(createdSlot);
            return createdSlot;
        }

        private void DestroySlot(UIInventorySlot slot)
        {
            _inventorySlots.Remove(slot);
            Destroy(slot.gameObject);
        }

        private void UpdateSlots()
        {
            foreach (var pair in _inventoryModel.InventoryDatas)
            {
                var slotIndex = pair.Key;
                var view = _inventorySlots[slotIndex];
                
                view.SetItem(pair.Value);
            }
            
        }
    }
}

