﻿using System;
using FlatLands.Architecture;
using FlatLands.Items;
using FlatLands.LocationsObjects;
using UnityEngine;

namespace FlatLands.Inventory
{
    public class InventoryManager : SharedObject, IObjectUseHandler
    {

        [Inject] private InventoryModel _inventoryModel;
        [Inject] private ItemsManager _itemsManager;
        private InventoryConfig _config;
        

        public override void Init()
        {
            _config = InventoryConfig.Instance;
            AddDefaultSlots();
        }

        public override void Dispose()
        {
            
        }

        private void AddDefaultSlots()
        {
            _inventoryModel.AddSlots(_config.SlotsCont);
            
        }

        #region IObjectUseHandler
        
        public Type UseType => typeof(ItemUseLocationObject);
        public void ObjetUse(ILocationObject locationObject)
        {
            if (locationObject is not ItemView itemView)
                return;
            var itemData = _itemsManager.GetDataByView(itemView);
            if (_inventoryModel.TryAddItem(itemData))
            {
                _itemsManager.DestroyItemView(itemData);
            }
        }
        
        #endregion
        
        
    }
}