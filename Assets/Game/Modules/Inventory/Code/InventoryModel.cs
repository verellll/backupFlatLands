﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlatLands.Architecture;
using FlatLands.Items;
using UnityEngine;

namespace FlatLands.Inventory
{
    public class InventoryModel : SharedObject
    {
        [Inject] private ItemsManager _itemsManager;
        
        private Dictionary<int, ItemData> _inventoryDatas = new Dictionary<int, ItemData>();
        public IReadOnlyDictionary<int, ItemData> InventoryDatas => _inventoryDatas;
        public int SlotsCount => _inventoryDatas.Count;
        public event Action OnSlotsChanged;
        public event Action OnItemsChanged;

        public override void Init()
        {
           
        }

        public override void Dispose()
        {
           
        }

        #region RemoveItem
        
        public bool CanRemoveItem(ItemData data)
        {
            return true;
        }
        
        public bool TryRemoveItem(ItemData data)
        {
            if (!CanRemoveItem(data))
                return false;

            OnItemsChanged?.Invoke();
            return true;
        }
        
        #endregion

        #region AddSlots

        public void AddSlots(int count)
        {
            var slotsCount = _inventoryDatas.Count;
            for (var i = slotsCount; i < count + slotsCount; i++)
            {
                _inventoryDatas[i] = null;
            }
            OnSlotsChanged?.Invoke();
        }

        public void RemoveSlots(int count)
        {
            for (int i = _inventoryDatas.Count - 1; i < count + _inventoryDatas.Count; i--)
            {
                //нужно дописать дроп предметов
                _inventoryDatas.Remove(i);
            }
            OnSlotsChanged?.Invoke();
        }
        
        #endregion

        #region Verifications

        //провера на пустые слоты
        private bool HaveEmptySlots()
        {
            var emptySlots = GetEmptySlots();
            return emptySlots.Count > 0;
        }
        
        private List<int> GetEmptySlots()
        {
            var emptySlots = new List<int>();
            foreach (var pair in _inventoryDatas)
            {
                var slotIndex = pair.Key;
                    var data = pair.Value;
                if (data == null)
                {
                    emptySlots.Add(slotIndex);
                }
            }
            return emptySlots;
        }

        //провера на похожие слоты
        private bool HaveSameSlots(ItemData data)
        {
            var sameSlots = GetSameSlots(data);
            return sameSlots.Count > 0;
        }
        
        private List<int> GetSameSlots(ItemData data)
        {
            var sameSlots = new List<int>();
            foreach (var pair in _inventoryDatas)
            {
                var slotIndex = pair.Key;
                var dataInSlot = pair.Value;
                if (dataInSlot != null && data == dataInSlot && CheckMaxCount(dataInSlot))
                {
                    sameSlots.Add(slotIndex);
                }
            }
            return sameSlots;
        }
        
        // проверка на заполненность
        private bool CheckMaxCount(ItemData data)
        {
            var maxCount = data.MaxCount;
            return data.Count != maxCount;
        }
        
        #endregion

        
        #region AddItem
        
        public bool CanAddItem(ItemData data)
        {
            return HaveSameSlots(data) || HaveEmptySlots();
        }
        
        public bool TryAddItem(ItemData data)
        {
            if (!CanAddItem(data))
            {
                return false;
            }
            if (HaveSameSlots(data))
            {
                if (TryAddInSameSlots(data))
                {
                    OnItemsChanged?.Invoke();
                    return true;
                }
                if (!HaveEmptySlots()) 
                    return false;
                if (AddInEmptySlots(data))
                {
                    OnItemsChanged?.Invoke();
                    return true;
                }
            }
            else if (HaveEmptySlots())
            {
                if (AddInEmptySlots(data))
                {
                    OnItemsChanged?.Invoke();
                    return true;
                }
            }
            return false;
        }

        public bool TryAddInSameSlots(ItemData data)
        {
            var count = data.Count;
            var maxCount = data.MaxCount;
            var sameSlots = GetSameSlots(data);
            for (var i = 0; i < sameSlots.Count; i++)
            {
                var dataInSlot = _inventoryDatas[i];
                var dataInSlotCount = dataInSlot.Count;
                var sum = count + dataInSlotCount;
                if (sum == maxCount || sum < maxCount)
                {
                    dataInSlot.Count = sum;
                    count = 0;
                    return true;
                }
                else
                {
                    dataInSlot.Count = maxCount;
                    data.Count = sum - maxCount;
                }
            }
            return count == 0;
        }

        public bool AddInEmptySlots(ItemData data)
        {
            var emptySlots = GetEmptySlots();
            var i = emptySlots.FirstOrDefault();
            _inventoryDatas[i] = data;
            return true;
        }
        

        #endregion
        


    }
}