using UnityEngine;

namespace FlatLands.LocationsObjects
{ 
	public interface ILocationObject
	{
		public int Id { get; }
		public GameObject LocationObject { get; }
	}
}
