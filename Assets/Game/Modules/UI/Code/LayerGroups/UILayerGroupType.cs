﻿namespace FlatLands.UI
{
    public enum UILayerGroupType
    {
        Hud = 10,
        Windows = 20,
        ItemDrag = 50,
    }
}