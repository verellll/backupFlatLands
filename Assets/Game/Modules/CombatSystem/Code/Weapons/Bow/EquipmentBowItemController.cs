﻿using FlatLands.Architecture;
using FlatLands.CharacterConstraints;
using FlatLands.CharacterLocomotion;
using FlatLands.Characters;
using FlatLands.Equipments;
using FlatLands.GeneralCamera;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    public sealed class EquipmentBowItemController : BaseEquipmentItemController
    {
        private int AnimatorBowLayerIndex => MainAnimator.GetLayerIndex("Bow_Layer");
        private int AnimatorBowVertical => Animator.StringToHash("BowVertical");
        
        [Inject] private GeneralCameraManager _generalCameraManager;
        [Inject] private CharactersManager _charactersManager;

        private bool IsAiming { get; set; }
        private new EquipmentBowItemConfig Config { get; }
        private new EquipmentBowItemBehaviour Behaviour { get; }

        private CharacterLocomotionProvider _characterLocomotionProvider;
        private CharacterConstraintsProvider _characterConstraintsProvider;
        
        private CharacterLocomotionType _locomotionTypeBeforeAiming;

        public EquipmentBowItemController(EquipmentBowItemConfig config, EquipmentBowItemBehaviour behaviour,
            Animator animator)
            : base(config, behaviour, animator)
        {
            Config = config;
            Behaviour = behaviour;
        }

        public override void Init()
        {
            _characterLocomotionProvider = _charactersManager.CurrentCharacter.GetProvider<CharacterLocomotionProvider>();
            _characterConstraintsProvider = _charactersManager.CurrentCharacter.GetProvider<CharacterConstraintsProvider>();
        }

        public override void MouseButtonDown(MouseButtonType buttonType)
        {
            if(buttonType != MouseButtonType.RightButton)
                return;

            EnterAiming();
        }

        public override void MouseButtonUp(MouseButtonType buttonType)
        {
            if(buttonType != MouseButtonType.RightButton)
                return;

            ExitAiming();
        }

        public override void HandleUpdate()
        {
            if(!IsAiming)
                return;
            
            MainAnimator.SetFloat(AnimatorBowVertical, _generalCameraManager.NormalizedXAngle);
        }

        private void EnterAiming()
        {
            if(IsAiming)
                return;

            _generalCameraManager.SetNewPivotPos(Config.AimingPivotPos, Config.AimingPivotPosDuration);
            _generalCameraManager.SetCustomFollowSpeed(Config.AimingCameraFollowSpeed);
            _generalCameraManager.SetCustomZoom(Config.AimingCameraZoom, Config.AimingCameraZoomDuration);
            _generalCameraManager.SetCustomXOffset(Config.AimingCameraXOffset, Config.AimingCameraXOffsetDuration);

            _locomotionTypeBeforeAiming = _characterLocomotionProvider.LocomotionType;
            _characterLocomotionProvider.SetLocomotionType(Config.AimingLocomotionType);
            
            MainAnimator.SetLayerWeight(AnimatorBowLayerIndex, 1, 0.3f);
            //_characterConstraintsProvider.SetGroupWeight(CharacterConstraintGroupType.Spine, 1, 0.5f);
            // MainAnimator.DoValue(AnimatorBowVertical, 1, 0.3f);
            
            IsAiming = true;
        }
        
        private void ExitAiming()
        {
            if(!IsAiming)
                return;

            _generalCameraManager.SetDefaultPivotPos();
            _generalCameraManager.SetDefaultFollowSpeed();
            _generalCameraManager.SetDefaultZoom();
            _generalCameraManager.SetDefaultXOffset();
            
            _characterLocomotionProvider.SetDefaultLocomotionType();
            MainAnimator.SetLayerWeight(AnimatorBowLayerIndex, 0, 0.3f);
            
            //_characterConstraintsProvider.SetDefaultGroupWeight(CharacterConstraintGroupType.Spine,  0.5f);
            // MainAnimator.DoValue(AnimatorBowVertical, 0, 0.2f);

            IsAiming = false;
        }
    }
}