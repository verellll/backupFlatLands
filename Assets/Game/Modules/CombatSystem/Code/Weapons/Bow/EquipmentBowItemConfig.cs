﻿using FlatLands.CharacterLocomotion;
using FlatLands.Equipments;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    [CreateAssetMenu(
        menuName = "FlatLands/Equipment/Items/" + nameof(EquipmentBowItemConfig), 
        fileName = nameof(EquipmentBowItemConfig))]
    public sealed class EquipmentBowItemConfig : BaseEquipmentItemConfig
    {
        [SerializeField, FoldoutGroup("Aim Settings")] 
        private CharacterLocomotionType _aimingLocomotionType;
        
        [SerializeField, FoldoutGroup("Aim Settings")] 
        private float _aimingChangeLocomotionTypeDuration = 0.4f;
        
        [SerializeField, FoldoutGroup("Aim Settings"), Space]
        private float _aimingCameraFollowSpeed = 0;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private float _aimingCameraZoom = -0.5f;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private float _aimingCameraZoomDuration = 0.2f;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private Vector3 _aimingPivotPos;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private float _aimingPivotPosDuraion = 0.1f;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private float _aimingCameraXOffset = 0.4f;
        
        [SerializeField, FoldoutGroup("Aim Settings")]
        private float _aimingCameraXOffsetDuration = 0.1f;

        public CharacterLocomotionType AimingLocomotionType => _aimingLocomotionType;
        public float AimingChangeLocomotionTypeDuration => _aimingChangeLocomotionTypeDuration;
        
        public float AimingCameraFollowSpeed => _aimingCameraFollowSpeed;
        public float AimingCameraZoom => _aimingCameraZoom;
        public float AimingCameraZoomDuration => _aimingCameraZoomDuration;
        public Vector3 AimingPivotPos => _aimingPivotPos;
        public float AimingPivotPosDuration => _aimingPivotPosDuraion;
        public float AimingCameraXOffset => _aimingCameraXOffset;
        public float AimingCameraXOffsetDuration => _aimingCameraXOffsetDuration;

        public override BaseEquipmentItemController CreateInstanceController(BaseEquipmentItemBehaviour behaviour, Animator animator)
        {
            return new EquipmentBowItemController(this, behaviour as EquipmentBowItemBehaviour, animator);
        }
    }
}