﻿using FlatLands.Equipments;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    [CreateAssetMenu(
        menuName = "FlatLands/Equipment/Items/" + nameof(EquipmentShieldItemConfig), 
        fileName = nameof(EquipmentShieldItemConfig))]
    public sealed class EquipmentShieldItemConfig : BaseEquipmentItemConfig
    {
        [SerializeField, FoldoutGroup("Combat Settings")]
        private string _enterBlockAnim;
        
        [SerializeField, FoldoutGroup("Combat Settings")]
        private string _exitBlockAnim;

        public string EnterBlockAnim => _enterBlockAnim;
        public string ExitBlockAnim => _exitBlockAnim;
        
        public override BaseEquipmentItemController CreateInstanceController(BaseEquipmentItemBehaviour behaviour, Animator animator)
        {
            return new EquipmentShieldItemController(this, behaviour as EquipmentShieldItemBehaviour, animator);
        }
    }
}