﻿using System.Collections;
using FlatLands.Architecture;
using FlatLands.Equipments;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    public sealed class EquipmentShieldItemController : BaseEquipmentItemController
    {
        public bool IsBlockActive { get; private set; }
        public new EquipmentShieldItemConfig Config { get; }
        public new EquipmentShieldItemBehaviour Behaviour { get; }

        private IEnumerator _blockRoutine;
        
        public EquipmentShieldItemController(EquipmentShieldItemConfig config, EquipmentShieldItemBehaviour behaviour,
	        Animator animator)
	        : base(config, behaviour, animator)
        {
	        Config = config;
	        Behaviour = behaviour;
        }
        
        public override void MouseButtonDown(MouseButtonType buttonType)
        {
	        if (buttonType != MouseButtonType.RightButton)
                return;

	        EnterBlock();
        }

        public override void MouseButtonUp(MouseButtonType buttonType)
        {
	        if(buttonType != MouseButtonType.RightButton)
                return;

            ExitBlock();
        }
        
	    private void EnterBlock()
	    {
		    if(IsBlockActive)
			    return;
		    
			IsBlockActive = true;
			_blockRoutine = MainAnimator.PlayAnimation(
				Config.AnimatorLayer,
				Config.EnterBlockAnim);
			UnityEventsProvider.CoroutineStart(_blockRoutine);
		}
		
		private void ExitBlock()
		{
			if (!IsBlockActive)
				return;
			
			_blockRoutine = MainAnimator.PlayAnimation(
				Config.AnimatorLayer,
				Config.ExitBlockAnim, () =>
				{
					IsBlockActive = false;
				});
			UnityEventsProvider.CoroutineStart(_blockRoutine);
		}
    }
}