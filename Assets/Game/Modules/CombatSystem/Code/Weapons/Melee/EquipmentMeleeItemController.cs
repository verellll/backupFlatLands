﻿using FlatLands.Equipments;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    public sealed class EquipmentMeleeItemController : BaseEquipmentItemController
    {
        public EquipmentMeleeItemController(EquipmentMeleeItemConfig config, EquipmentMeleeItemBehaviour behaviour, Animator animator) 
            : base(config, behaviour, animator) { }
        
        public override void MouseButtonDown(MouseButtonType buttonType)
        {
            
        }

        public override void MouseButtonUp(MouseButtonType buttonType)
        {
            
        }

        public override void MouseButtonHold(MouseButtonType buttonType)
        {
            
        }
    }
}