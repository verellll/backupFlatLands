﻿using FlatLands.Equipments;
using UnityEngine;

namespace FlatLands.CombatSystem
{
    [CreateAssetMenu(
        menuName = "FlatLands/Equipment/Items/" + nameof(EquipmentMeleeItemConfig), 
        fileName = nameof(EquipmentMeleeItemConfig))]
    public sealed class EquipmentMeleeItemConfig : BaseEquipmentItemConfig
    {
        public override BaseEquipmentItemController CreateInstanceController(BaseEquipmentItemBehaviour behaviour, Animator animator)
        {
            return new EquipmentMeleeItemController(this, behaviour as EquipmentMeleeItemBehaviour, animator);
        }
    }
}