using UnityEngine;

namespace FlatLands.CombatSystem
{
	public abstract class BaseCombatProvider
	{
		protected BaseCombatBehaviour _combatBehaviour;
		protected Animator _animator;
		
		public BaseCombatProvider(BaseCombatBehaviour combatBehaviour, Animator animator)
		{
			_combatBehaviour = combatBehaviour;
			_animator = animator;
		}
	}
}
