using System;
using System.Collections;
using System.Collections.Generic;
using FlatLands.Architecture;
using FlatLands.Locations;
using FlatLands.LocationsCamera;
using UnityEngine;

namespace FlatLands.Items
{
    public class ItemsManager : SharedObject
    {
        [Inject] private LocationsManager _locationsManager;
        [Inject] private LocationsCameraManager _locationsCameraManager;
        
        private Transform _dropContainer;
        
        public Dictionary<int,ItemData> ItemsData { get; private set; }

        private List<ItemView> _itemsViewsOnScene;
        private int _itemsCounter;
        
        public override void Init()
        {
            // _locationsCameraManager.OnLocationObjectHit += 
            
            ItemsData = new Dictionary<int, ItemData>();
            ItemConfig.Init();
            var configs = ItemConfig.ByName;
            var itemsLocationData = _locationsManager.CurLocation.Item2.GetData<ItemsLocationData>();
            _dropContainer = itemsLocationData.DropContainer;
            _itemsViewsOnScene = itemsLocationData.itemsViewsOnScene;
            GetItemsDataOnScene();
        }

        private void GetItemsDataOnScene()
        {
            if (_itemsViewsOnScene == null)
                return;
            
            foreach (var view in _itemsViewsOnScene)
            {
                var config = view.Config;
                var count = view.Count;
                var newData = CreatItemData(config, count);
                view.SetId(newData.Id);
                newData.ItemView = view;
            }
        }
        
        public ItemData CreatItemData(ItemConfig config, int startCount)
        {
            var id = CreatItemId();
            var newData = new ItemData(id, config, startCount);
            ItemsData[id] = newData;
            return newData;
        }

        public void DestroyItemData(int id)
        {
            ItemsData.Remove(id);
        }

        public ItemView CreatItemView(int id, ItemData itemData, Vector3 position)
        {
            var prefab = itemData.Config.ItemPrefab;
            var itemView = GameObject.Instantiate(prefab,position, Quaternion.identity);
            itemData.ItemView = itemView;
            itemView.SetId(id); 
            return itemView;
        }

        public void DestroyItemView(ItemData itemData)
        {
            Debug.Log(itemData.HaveView);
            if (!itemData.HaveView)
                return;
            GameObject.Destroy(itemData.ItemView.gameObject);
            itemData.ItemView = null;
        }

        private int CreatItemId()
        {
            return _itemsCounter++;
        }

        public ItemData GetDataById(int id)
        {
            if(!ItemsData.TryGetValue(id, out var itemData))
                return default;
            return itemData;
        }

        public ItemData GetDataByView(ItemView itemView)
        {
            var id = itemView.Id;
            var data = GetDataById(id);
            return data;
        }
        
    }
}
