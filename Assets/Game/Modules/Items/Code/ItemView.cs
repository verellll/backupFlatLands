using FlatLands.LocationsObjects;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.Items
{
    public class ItemView : MonoBehaviour, ILocationObject
    {
        [SerializeField]
        private ItemConfig _itemConfig;

        [SerializeField]
        private int _count = 1;
        
        public ItemConfig Config => _itemConfig;
        public GameObject LocationObject => gameObject;
        public int Count => _count;
        public int Id { get; private set; }

        internal void SetId(int id)
        {
            Id = id;
        }
    }
}