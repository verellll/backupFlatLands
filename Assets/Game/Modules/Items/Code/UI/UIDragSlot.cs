﻿using UnityEngine;

namespace FlatLands.Items
{
    public class UIDragSlot : UIItemSlot
    {
        public override bool CanDrag => false;

        public RectTransform Rect
        {
            get
            {
                if (_rect == null)
                    _rect = GetComponent<RectTransform>();

                return _rect;
            }
        }
        private RectTransform _rect;
        
    }
}