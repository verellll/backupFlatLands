﻿using FlatLands.Architecture;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FlatLands.Items
{
    public abstract class UIItemSlot : SerializedMonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [Inject] private ItemsDragManager _itemsDragManager;
        
        [SerializeField, BoxGroup("Base Settings")] 
        private Image _icon;

        [SerializeField, BoxGroup("Base Settings")] 
        private TMP_Text _countText;

        [SerializeField, BoxGroup("Base Settings")] 
        private GameObject _countContainer;
        public ItemData CurrentItem { get; private set; }

        public abstract bool CanDrag { get; }

        public virtual void SetItem(ItemData data)
        {
            CurrentItem = data;
            UpdateState();
        }

        protected virtual void UpdateState()
        {
            var hasItem = CurrentItem != null;
            if (hasItem)
            {
                _icon.gameObject.SetActive(true);
                _icon.sprite = CurrentItem.Config.Icon;

                var count = CurrentItem.Count;
                if (count > 1)
                {
                    _countContainer.gameObject.SetActive(true);
                    _countText.text = count.ToString();
                }
                else
                {
                    _countContainer.gameObject.SetActive(false);
                }
            }
            else
            {
                _icon.gameObject.SetActive(false);
                _countContainer.gameObject.SetActive(false);
            }
        }
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!CanDrag || CurrentItem == null) return;
            _itemsDragManager.OnBeginDrag(eventData, this);
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!CanDrag) return;
            _itemsDragManager.OnDrag(eventData, this);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (!CanDrag) return;
            _itemsDragManager.OnEndDrag(eventData, this);
        }
    }
}