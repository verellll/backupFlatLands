﻿using FlatLands.UI;
using UnityEngine;

namespace FlatLands.Items
{
    public  class UIItemsDragElement : UIElement
    {
        [SerializeField] 
        private UIDragSlot _dragSlot;

        public UIDragSlot DragSlot => _dragSlot;
    }
}