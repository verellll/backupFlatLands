﻿using FlatLands.Architecture;
using FlatLands.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FlatLands.Items
{
    public class ItemsDragManager : SharedObject
    {
        private const int LeftMouseButton = 0;
        [Inject] private UIManager _uiManager;
        private UIDragSlot _dragSlot;
        
        public override void Init()
        {
            if(!_uiManager.Hierarchy.LayerGroups.TryGetValue(UILayerGroupType.ItemDrag, out var dragLayer))
                return;

            var dragUIElement = dragLayer.GetUIElement<UIItemsDragElement>();
            _dragSlot = dragUIElement.DragSlot;
        }

        internal void OnBeginDrag(PointerEventData eventData, UIItemSlot selectedSlot)
        {
            if (eventData.button != LeftMouseButton) return;
            var dataInSlot = selectedSlot.CurrentItem;
            _dragSlot.SetItem(dataInSlot);
            selectedSlot.SetItem(null);
            _dragSlot.gameObject.SetActive(true);
        }

        internal void OnDrag(PointerEventData eventData, UIItemSlot selectedSlot)
        {
            var uiCamera = _uiManager.Hierarchy.UICamera;
            var mousePos = Input.mousePosition;
            var canvas = _uiManager.Hierarchy.UICanvas;
            var slotWorldPos = uiCamera.ScreenToWorldPoint(mousePos);
            _dragSlot.transform.position = slotWorldPos;
            /*_dragSlot.Rect.anchoredPosition += eventData.delta / canvas.scaleFactor;*/
          
        }

        internal void OnEndDrag(PointerEventData eventData, UIItemSlot selectedSlot)
        {
            if (eventData.button != LeftMouseButton) return;
            Debug.Log("End");
            _dragSlot.gameObject.SetActive(false);
        }
    }
}