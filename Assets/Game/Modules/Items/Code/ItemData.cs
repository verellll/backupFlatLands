using Sirenix.OdinInspector;
using UnityEngine;

namespace FlatLands.Items
{
    public class ItemData
    {
        public int Id { get; }
        public ItemConfig Config { get; }
        public int Count { get; set; }
        
        public int MaxCount { get; set; }
        public ItemView ItemView { get; set; }
        
        public bool HaveView => ItemView != null;
    
        public ItemData (int id, ItemConfig itemConfig, int startCount)
        {
            Id = id;
            Config = itemConfig;
            Count = startCount;
        }
    }
    
}